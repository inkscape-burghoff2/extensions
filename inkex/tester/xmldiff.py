#
# Copyright 2011 (c) Ian Bicking <ianb@colorstudy.com>
#           2019 (c) Martin Owens <doctormo@gmail.com>
#
# Taken from http://formencode.org under the GPL compatible PSF License.
# Modified to produce more output as a diff.
#
"""
Allow two xml files/lxml etrees to be compared, returning their differences.
"""

import xml.etree.ElementTree as xml
from io import BytesIO

from inkex.paths import Path
from lxml import etree
from inkex import addNS
import re


def text_compare(test1, test2):
    """
    Compare two text strings while allowing for '*' to match
    anything on either lhs or rhs.
    """
    if not test1 and not test2:
        return True
    if test1 == "*" or test2 == "*":
        return True
    return (test1 or "").strip() == (test2 or "").strip()


class DeltaLogger(list):
    """A record keeper of the delta between two svg files"""

    def append_tag(self, tag_a, tag_b, ida, idb):
        """Record a tag difference"""
        if tag_a:
            tag_a = f"<{tag_a}.../>"
        if tag_b:
            tag_b = f"<{tag_b}.../>"
        self.append(((ida, tag_a), (idb, tag_b)))

    def append_attr(self, attr, value_a, value_b, a_id, b_id):
        """Record an attribute difference"""

        def _prep(val, idv):
            if val:
                if attr == "d":
                    return [attr] + Path(val).to_arrays()
                return (idv, attr, val)
            return val

        # Only append a difference if the preprocessed values are different.
        # This solves the issue that -0 != 0 in path data.
        prep_a = _prep(value_a, a_id)
        prep_b = _prep(value_b, b_id)
        if prep_a != prep_b:
            self.append((prep_a, prep_b))

    def append_text(self, text_a, text_b, ida, idb):
        """Record a text difference"""
        self.append(((ida, text_a), (idb, text_b)))

    def __bool__(self):
        """Returns True if there's no log, i.e. the delta is clean"""
        return not self.__len__()

    __nonzero__ = __bool__

    def __repr__(self):
        if self:
            return "No differences detected"
        return f"{len(self)} xml differences"


def to_xml(data):
    """Convert string or bytes to xml parsed root node"""
    if isinstance(data, str):
        data = data.encode("utf8")
    if isinstance(data, bytes):
        return xml.parse(BytesIO(data)).getroot()
    return data


def to_lxml(data):
    """Convert string or bytes to lxml parsed root node"""
    if isinstance(data, str):
        data = data.encode("utf8")
    if isinstance(data, bytes):
        return etree.parse(BytesIO(data)).getroot()
    return data


def xmldiff(data1, data2):
    """Create an xml difference, will modify the first xml structure with a diff"""
    xml1, xml2 = to_lxml(data1), to_lxml(data2)

    # Standardize all IDs in document with a test ID related to the xpath location
    # The memos are dicts that can be used to look up the original ID from the test ID
    memo1 = xpath_ids(xml1)
    memo2 = xpath_ids(xml2)

    # Log deltas, reporting the original ID when possible
    delta = DeltaLogger()
    _xmldiff(xml1, xml2, delta, memo1, memo2)
    return xml.tostring(xml1).decode("utf-8"), delta


def xpath_ids(svg):
    """
    When comparing two documents, replace all IDs with a test ID related to the xpath location
    Some of what's used here may duplicate Inkex functionality, but this is necessary since
    testing should largely be Inkex-independent
    """

    # Prune comments for compatibility with the XML parser, which does not parse comments
    ctag = etree.Comment
    for el in svg.iter():
        if el.tag == ctag:
            el.getparent().remove(el)

    # Identify all the xpaths
    siter = svg.iter()
    next(siter, None)
    xps = {svg: ""}
    for el in siter:
        myp = el.getparent()
        xps[el] = xps[myp] + "/{0}".format(myp.index(el) + 1)
        # Simplified version of true xpath for brevity

    urlpat = r"url\(#[^\)]*\)"  # URL pattern
    csspat = r"[\.\#](.*?)[\{\,]"  # CSS class/id pattern

    hrefs = {addNS("href", "xlink"), "href"}
    bareids = {addNS("stockid", "inkscape"), "class"}
    styles = {addNS("style", "svg"), "style"}

    # Collect all ids present in document in order and decide replacement values
    repl_ids = dict()
    fid = lambda id1, id2: f"xpId{id1}{'_' if id2 else ''}{id2}"
    for el in svg.iter():
        oldid = el.get("id")
        xp = xps[el]
        if oldid is None:
            # No ids: assign one
            newid = fid(xp, "")
            el.set("id", newid)
        elif not (oldid.startswith("xpId")) and oldid not in repl_ids:
            # Id not already replaced: decide replacement
            repl_ids[oldid] = fid(xp, "")

    # Now, find any references in attributes/text that were not already assigned new IDs
    for el in svg.iter():
        count = 0
        xp = xps[el]
        for n, v in el.attrib.items():
            # ids in xlink:href values
            if v.startswith("#") and n in hrefs:
                oldid = v[1:]
                if oldid not in repl_ids:
                    repl_ids[oldid] = fid(xp, count)
                    count += 1
            elif n in bareids:
                oldid = v
                if oldid not in repl_ids:
                    repl_ids[oldid] = fid(xp, count)
                    count += 1
            # ids in url(#)'s
            elif v is not None and len(re.findall(urlpat, v)) > 0:
                ms = re.findall(urlpat, v)
                for m in ms:
                    oldid = m[5:-1]
                    if oldid not in repl_ids:
                        repl_ids[oldid] = fid(xp, count)
                        count += 1
        if el.tag in styles:
            # ids in css style text url(#)'s
            if el.text is not None and len(re.findall(urlpat, el.text)) > 0:
                ms = re.findall(urlpat, el.text)
                for m in ms:
                    oldid = m[5:-1]
                    if oldid not in repl_ids:
                        repl_ids[oldid] = fid(xp, count)
                        count += 1

            # class ids in css style text .id{
            if el.text is not None and len(re.findall(csspat, el.text)) > 0:
                ms = re.findall(csspat, el.text)
                for m in ms:
                    oldid = m.strip()
                    if oldid not in repl_ids:
                        repl_ids[oldid] = fid(xp, count)
                        count += 1

    # Make all replacements
    oldids2 = {"#" + v for v in repl_ids.keys()}  # oldids with #
    oldids3 = {"url(#" + v + ")" for v in repl_ids.keys()}  # oldids with url(#...)
    for el in svg.iter():
        for n, v in el.attrib.items():
            if n == "id" and v in repl_ids:
                el.attrib[n] = repl_ids[v]
            elif v in oldids2 and n in hrefs:
                el.attrib[n] = "#" + repl_ids[v[1:]]
            elif n in bareids:
                el.attrib[n] = repl_ids[v]
            elif "url(#" in v:  # precheck for speed
                ms = re.findall(urlpat, v)
                for m in ms:
                    if m in oldids3:
                        el.attrib[n] = el.attrib[n].replace(
                            m, "url(#" + repl_ids[m[5:-1]] + ")"
                        )
        if el.tag in styles:
            if el.text is not None:
                for w in [oi3 for oi3 in oldids3 if oi3 in el.text]:
                    el.text = el.text.replace(
                        "url(#" + w[5:-1] + ")", "url(#" + repl_ids[w[5:-1]] + ")"
                    )
                ms = re.findall(csspat, el.text)
                for m in ms:
                    if m.strip() in repl_ids:
                        el.text = el.text.replace(m, repl_ids[m.strip()]) + " "
    memo = {v: k for k, v in repl_ids.items()}
    return memo


def _xmldiff(xml1, xml2, delta, memo1, memo2):
    id1 = memo1.get(xml1.get("id"), xml1.get("id"))
    id2 = memo2.get(xml2.get("id"), xml2.get("id"))
    if xml1.tag != xml2.tag:
        delta.append_tag(xml1.tag, xml2.tag, id1, id2)
    for name, value in xml1.attrib.items():
        if name not in xml2.attrib:
            delta.append_attr(name, xml1.attrib[name], None, id1, id2)
            xml1.attrib[name] += "XXX"
        elif xml2.attrib.get(name) != value:
            delta.append_attr(
                name, xml1.attrib.get(name), xml2.attrib.get(name), id1, id2
            )
            xml1.attrib[name] = f"{xml1.attrib.get(name)}XXX{xml2.attrib.get(name)}"
    for name, value in xml2.attrib.items():
        if name not in xml1.attrib:
            delta.append_attr(name, None, value, id1, id2)
            xml1.attrib[name] = "XXX" + value
    if not text_compare(xml1.text, xml2.text):
        delta.append_text(xml1.text, xml2.text, id1, id2)
        xml1.text = f"{xml1.text}XXX{xml2.text}"
    if not text_compare(xml1.tail, xml2.tail):
        delta.append_text(xml1.tail, xml2.tail, id1, id2)
        xml1.tail = f"{xml1.tail}XXX{xml2.tail}"

    # Get children and pad with nulls
    children_a = list(xml1)
    children_b = list(xml2)
    children_a += [None] * (len(children_b) - len(children_a))
    children_b += [None] * (len(children_a) - len(children_b))

    for child_a, child_b in zip(children_a, children_b):
        if child_a is None:  # child_b exists
            delta.append_tag(
                None, child_b.tag, None, memo2.get(child_b.get("id"), child_b.get("id"))
            )
        elif child_b is None:  # child_a exists
            delta.append_tag(
                child_a.tag, None, memo1.get(child_a.get("id"), child_a.get("id")), None
            )
        else:
            _xmldiff(child_a, child_b, delta, memo1, memo2)
